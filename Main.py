#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import time

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QColor, QPalette
from PyQt5.QtWidgets import QApplication, qApp, QSpacerItem, QSizePolicy
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QGridLayout,  QHBoxLayout, QGroupBox, QDialog, QVBoxLayout
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtGui import QPainter, QColor, QPen

from qroundprogressbar2 import QRoundProgressBar2

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 image - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 840
        self.height = 480
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        bBrush = QBrush(QColor("#F29400"))
        eBrush = QBrush(QColor("#cc7c00"))
        hlayout = QHBoxLayout()

        grid_layout = QGridLayout()
        grid_layout.setSpacing(0)

        # Create widget
        label = QLabel()
        pixmap = QPixmap('roskilde_webtop_2.jpg')
        label.setPixmap(pixmap)

        lWaste = QLabel()
        lCo = QLabel()

        lWaste.setText("Kg of Waste")
        lWaste.setFont(QFont("Hack", 24, QFont.Bold))
        lWaste.heightForWidth(10)
        lWaste.setStyleSheet('color: #FFEBCD; text-align:center')
        lWaste.setFixedHeight(80)

        lCo.setText("Bio Fiuel")
        lCo.setFont(QFont("Hack", 24, QFont.Bold))
        lCo.heightForWidth(10)
        lCo.setStyleSheet('color: #FFEBCD')
        lCo.setFixedHeight(80)

        progress = QRoundProgressBar2()
        progress.setBarStyle(QRoundProgressBar2.BarStyle.DONUT)
        palette = QPalette()
        brush = QBrush(QColor("#FFEBCD"))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Highlight, brush)
        palette.setBrush(QPalette.Highlight, brush)
        palette.setBrush(QPalette.AlternateBase, bBrush)
        palette.setBrush(QPalette.Base, eBrush)
        palette.setBrush(QPalette.Shadow, bBrush)
        progress.setPalette(palette)

        progress2 = QRoundProgressBar2()

        progress2.setBarStyle(QRoundProgressBar2.BarStyle.DONUT)
        palette2 = QPalette()
        brush2 = QBrush(QColor(0,119,117))
        brush2.setStyle(Qt.SolidPattern)
        palette2.setBrush(QPalette.Highlight, brush2)
        palette2.setBrush(QPalette.AlternateBase, bBrush)
        palette2.setBrush(QPalette.Base, eBrush)
        palette2.setBrush(QPalette.Shadow, bBrush)
        progress2.setPalette(palette2)


        grid_layout.addWidget(lWaste, 0, 0, Qt.AlignCenter)
        grid_layout.addWidget(lCo, 0, 2, Qt.AlignCenter)
        grid_layout.addWidget(progress, 1, 0)
        grid_layout.addWidget(progress2, 1, 2)
        grid_layout.addWidget(label, 1, 1)

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), QColor(242, 148, 0))
        self.setPalette(p)
        self.setLayout(grid_layout)

        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
